#pragma once

#include "ofMain.h"
#include "ofxAssimpModelLoader.h"
#include "ofxAzureKinect.h"

class ofApp 
	: public ofBaseApp 
{
public:
	void setup();
	void exit();

	void update();
	void draw();



private:
	ofxAzureKinect::Device kinectDevice;
	ofxAssimpModelLoader model;
	ofxAssimpModelLoader flower;
	ofLight	light;

	ofEasyCam camera;

	ofVboMesh skeletonMesh;
};
